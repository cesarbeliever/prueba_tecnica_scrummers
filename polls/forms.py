from django import forms
from django.forms import ModelForm
from .models import *



class IntegrantesForm(ModelForm):
   
   class Meta:
      model = Integrantes
      fields = ['imagen','nombres','apellidos','descripcion','grupodetrabajo']    
      widgets = {
       'imagen': forms.FileInput(attrs={'class':'input100','placeholder':'Imagen'}), 
       'nombres': forms.TextInput(attrs={'class':'input100','placeholder':'Nombres'}),
       'apellidos': forms.TextInput(attrs={'class':'input100','placeholder':'Apellidos'}),
       'descripcion': forms.TextInput(attrs={'class':'input100','placeholder':'Describete'}),
       'grupodetrabajo': forms.Select(attrs={'class':'input100','placeholder':'Grupo'}),

      }



class GruposForm(ModelForm):
   
   class Meta:
      model = Grupodetrabajo
      fields = ['nombre','maxnumero','minnumero']    
      widgets = {
       'nombre': forms.TextInput(attrs={'class':'input100','placeholder':'Nombre'}),
       'maxnumero': forms.NumberInput(attrs={'class':'input100','placeholder':'Integrantes maximo'}),
       'minnumero': forms.NumberInput(attrs={'class':'input100','placeholder':'Integrantes minimo'}),

      }