# Generated by Django 3.2.3 on 2021-05-22 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0005_integrantes_descripcion'),
    ]

    operations = [
        migrations.AddField(
            model_name='integrantes',
            name='imagen',
            field=models.ImageField(default=1, upload_to=''),
            preserve_default=False,
        ),
    ]
