from django.db import models
from smart_selects.db_fields import ChainedForeignKey

# Create your models here.


class Grupodetrabajo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=30, unique=True )
    maxnumero = models.IntegerField()
    minnumero = models.IntegerField()
    def __str__(self):
        return self.nombre 
    @property
    def get_integrantes(self):
        integrantes = self.integrantes_set.all()
        
        return integrantes

class Integrantes(models.Model):
    id = models.AutoField(primary_key=True)
    imagen = models.ImageField()
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    descripcion = models.TextField(max_length=300)
    grupodetrabajo = models.ForeignKey(Grupodetrabajo, on_delete=models.CASCADE, null=True, to_field='nombre')
    def __str__(self):
        return self.nombres  