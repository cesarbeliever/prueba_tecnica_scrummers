from django.urls import path, include
from django.conf.urls import url
from . import views
from .views import *
from django.conf.urls.static import static
from django.conf import settings



urlpatterns = [
    path('',inicio, name='inicio'),
    path('inicio/',inicio, name='inicio'),

    path('editarintegrante/<str:pk>/', editarintegrante, name='editarintegrante'),
    path('eliminarintegrante/<str:pk>/', eliminarintegrante, name='eliminarintegrante'),
    path('agregarintegrante/', agregarintegrante, name='agregarintegrante'),
    path('agregargrupo/', agregargrupo, name='agregargrupo'),


]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)