from polls.models import *
from django.shortcuts import render, redirect
from .forms import  *
from django.contrib import messages

# Create your views here.
def inicio(request):
  model=Grupodetrabajo.objects.filter()
  model2=Integrantes.objects.filter().values('id','nombres','apellidos','descripcion','grupodetrabajo','imagen')
 
  return render(request, 'index.html',{'model':model,'model2':model2})


def eliminarintegrante(request, pk):
	integrante = Integrantes.objects.get(id=pk)	
	integrante.delete()
	return redirect('inicio')


def editarintegrante(request, pk):
  integrante = Integrantes.objects.get(id=pk)
  form = IntegrantesForm(instance=integrante)
  if request.method == 'POST':
    integrante.nombres = request.POST["nombres"]
    integrante.apellidos = request.POST["apellidos"]
    integrante.descripcion = request.POST["descripcion"]
    integrante.imagen = request.FILES["imagen"]
    grupo = Grupodetrabajo.objects.get(nombre=request.POST["grupodetrabajo"])
    integrante.grupodetrabajo = grupo
    integrante.save()
    messages.success(request,"Cambios guardados")
    return redirect('inicio')

  context = {'form':form}
  return render(request, 'Registrointegrantes.html', context)

def agregarintegrante(request):
  form = IntegrantesForm()
  if request.method == 'POST':
    grupo = Grupodetrabajo.objects.get(nombre=request.POST["grupodetrabajo"])
    newintegrante = Integrantes.objects.create(nombres=request.POST["nombres"],apellidos=request.POST["apellidos"],descripcion=request.POST["descripcion"],imagen=request.FILES["imagen"],grupodetrabajo=grupo)
    return redirect('inicio')

  context = {'form':form}
  return render(request, 'Registrointegrantes.html', context)


def agregargrupo(request):
  form = GruposForm()
  if request.method == 'POST':
    form = GruposForm(request.POST)
    if form.is_valid():
        form.save()
        messages.success(request,"Cambios guardados")
        return redirect('inicio')
    messages.success(request,"Cambios no guardados")
    return redirect('inicio')

  context = {'form':form}
  return render(request, 'Registrogrupo.html', context)