from django.contrib import admin
from django.urls import path, include
from django.contrib import admin
from django.urls import include, path
from django.conf import settings

urlpatterns = [
    
    path('', include('polls.urls')),
    
    path('admin/', admin.site.urls),
    
    
] 

#Add Django site authentication urls (for login, logout, password management)
urlpatterns += [
    path('', include('django.contrib.auth.urls')),
]
